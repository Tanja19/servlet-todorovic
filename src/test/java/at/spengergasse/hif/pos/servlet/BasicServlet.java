package at.spengergasse.hif.pos.servlet;

import javax.servlet.GenericServlet;
import javax.servlet.Servlet;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;


public class BasicServlet extends GenericServlet implements Servlet {

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServerException, IOException {

        PrintWriter writer = response.getWriter();
        writer.println("<html>");
        writer.println("<head>");
        writer.println("<title>Hip Hip Hurra</title>");
        writer.println("</head>");
    }
}